<?php

namespace Tests\Unit;

use App\Models\EquipmentInventory;
use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Prepare\Orders;
use Tests\TestCase;

class CalendarTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMakeSureTimeLineReturnData()
    {
        list($order, $equipmentInventory, $equipmentInventory2) = Orders::create();

        // assertions
        // order
        $foundOrder = Order::find($order->id);
        $this->assertNotNull($foundOrder);
        $this->assertEquals($order->id, $foundOrder->id);

        // inventory
        $foundEI = EquipmentInventory::find($equipmentInventory->id);
        $this->assertNotNull($foundEI);
        $this->assertEquals($equipmentInventory->id, $foundEI->id);

        // return inventory
        $foundEI2 = EquipmentInventory::find($equipmentInventory2->id);
        $this->assertNotNull($foundEI2);
        $this->assertEquals($equipmentInventory2->id, $foundEI2->id);
    }
}
