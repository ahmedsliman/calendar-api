<?php

namespace Tests\Unit\Models;

use App\Models\Station;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testStationModelCanRetrieve()
    {
        // prepare
        $station = Station::factory()->create();

        // asserts
        $foundStation = Station::find($station->id);

        $this->assertNotNull($foundStation);
        $this->assertEquals($station->name, $foundStation->name);
    }
}
