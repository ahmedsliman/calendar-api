<?php

namespace Tests\Unit\Models;

use App\Models\Camp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CampTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testCampModelCanRetrieve()
    {
        // prepare
        $camp = Camp::factory()->create();

        // asserts
        $foundCamp = Camp::find($camp->id);

        $this->assertNotNull($foundCamp);
        $this->assertEquals($camp->name, $foundCamp->name);
    }
}
