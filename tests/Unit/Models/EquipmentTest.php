<?php

namespace Tests\Unit\Models;

use App\Models\Equipment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EquipmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testEquipmentModelCanRetrieve()
    {
        // prepare
        $equipment = Equipment::factory()->create();

        // asserts
        $foundEquipment = Equipment::find($equipment->id);

        $this->assertNotNull($foundEquipment);
        $this->assertEquals($equipment->name, $foundEquipment->name);
    }
}
