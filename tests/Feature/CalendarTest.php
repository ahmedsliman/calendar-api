<?php

namespace Tests\Feature;

use App\Util\InventoryUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\Prepare\Orders;
use Tests\TestCase;

class CalendarTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMakeSureTimeLineReturnCorrectDataForStartStation()
    {
        // prepare
        list($order, $equipmentInventory, $equipmentInventory2) = Orders::create();

        // request
        $response = $this->get('/api/calendar/'.$equipmentInventory->station_id);

        // Assertions
        $afterWithdraw = InventoryUtil::INITIAL_QUANTITY + intval($equipmentInventory->quantity);

        $response->assertStatus(200);
        $response->assertSee($order->rental_start_date);
        $response->assertSee($order->quantity);
        $response->assertStatus(200);
        $response->assertSeeTextInOrder([
            'today',
            $equipmentInventory->quantity,
            'balance',
            $afterWithdraw
        ]);
    }

    /**
     * @return void
     */
    public function testMakeSureTimeLineReturnCorrectDataForReturnStation()
    {
        // prepare
        list($order, $equipmentInventory, $equipmentInventory2) = Orders::create();

        // request
        $response = $this->get('/api/calendar/'.$equipmentInventory2->station_id);

        // Assertions
        $afterReturn = InventoryUtil::INITIAL_QUANTITY + intval($equipmentInventory2->quantity);

        $response->assertStatus(200);
        $response->assertSee($order->rental_end_date);
        $response->assertSee($order->quantity);
        $response->assertStatus(200);
        $response->assertSeeTextInOrder([
            'today',
            $equipmentInventory2->quantity,
            'balance',
            $afterReturn
        ]);
    }

}
