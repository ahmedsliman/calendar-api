<?php

namespace Tests\Feature\Models;

use App\Models\Equipment;
use App\Models\Station;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testStationsListReturnValuesInProperSigniture()
    {
        // prepare
        $station = Station::factory()->create(['name' => 'station for testing!']);

        // request
        $response = $this->get('/api/stations');

        // assertions
        $response->assertStatus(200);
        $response->assertJsonFragment(['id' => $station->id, 'name' => 'station for testing!']);
    }
}
