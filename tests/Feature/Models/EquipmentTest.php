<?php

namespace Tests\Feature\Models;

use App\Models\Equipment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EquipmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testEquipmentsListReturnValuesInProperSigniture()
    {
        // prepare
        $equipment = Equipment::factory()->create(['name' => 'equipment for testing!']);

        // request
        $response = $this->get('/api/equipments');

        // assertions
        $response->assertStatus(200);
        $response->assertJsonFragment(['id' => $equipment->id, 'name' => 'equipment for testing!']);
    }
}
