<?php

namespace Tests\Feature\Models;

use App\Models\Camp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CampTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test void
     */
    public function testCampsListReturnValuesInProperSigniture()
    {
        // prepare
        $camp = Camp::factory()->create(['name' => 'camp for testing!']);

        // request
        $response = $this->get('/api/camps');

        // assertions
        $response->assertStatus(200);
        $response->assertJsonFragment(['id' => $camp->id, 'name' => 'camp for testing!']);
    }
}
