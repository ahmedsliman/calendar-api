<?php

namespace Tests\Prepare;

use App\Models\Camp;
use App\Models\Equipment;
use App\Models\EquipmentInventory;
use App\Models\Order;
use App\Models\Station;
use Carbon\Carbon;

class Orders
{
    /**
     * @return array
     */
    public static function create(): array
    {
        // prepare
        $order = Order::factory()->create([
            'camp_id' => Camp::factory()->create()->id,
            'start_station_id' => $startStation = Station::factory()->create()->id,
            'end_station_id' => $returnStation = Station::factory()->create()->id,
            'rental_start_date' => $startDate = Carbon::today()->addDays(rand(2, 3))->toDateString(),
            'rental_end_date' => $endDate = Carbon::today()->addDays(rand(5, 7))->toDateString(),
        ]);
        $equipmentInventory = EquipmentInventory::factory()->create([
            'order_id' => $order->id,
            'order_date' => $startDate,
            'station_id' => $startStation,
            'equipment_id' => $equipmentId = Equipment::factory()->create()->id,
            'quantity' => -5,
        ]);
        $equipmentInventory2 = EquipmentInventory::factory()->create([
            'order_id' => $order->id,
            'order_date' => $endDate,
            'station_id' => $returnStation,
            'equipment_id' => $equipmentId,
            'quantity' => 5,
        ]);

        return [$order, $equipmentInventory, $equipmentInventory2];
    }
}
