<?php

use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CampController;
use App\Http\Controllers\EquipmentController;
use App\Http\Controllers\StationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('stations', [StationController::class, 'index']);
Route::get('camps', [CampController::class, 'index']);
Route::get('equipments', [EquipmentController::class, 'index']);
Route::get('calendar/{station}', [CalendarController::class, 'timeline']);
