<?php

namespace App\Util;

class ResponseStatusCodeUtil
{
    CONST HTTP_OK = 200;
    CONST HTTP_CREATED = 201;
    CONST HTTP_UNAUTHORIZED = 401;
    CONST HTTP_NOT_FOUND = 404;
    CONST HTTP_FORBIDDEN = 403;
    CONST HTTP_BAD_REQUEST = 400;
    CONST HTTP_UNPROCESSABLE_ENTITY = 422;
    CONST HTTP_CONFLICT = 409;
    CONST HTTP_INTERNAL_SERVER_ERROR = 500;
}
