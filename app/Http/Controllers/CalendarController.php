<?php

namespace App\Http\Controllers;

use App\DTO\CalendarDTO;
use App\Http\Responses\CalendarResponse;
use App\Models\Station;
use App\Service\CalendarService;

class CalendarController extends Controller
{
    /**
     * @var CalendarService
     */
    private CalendarService $calendarService;

    public function __construct(CalendarService $calendarService)
    {
        $this->calendarService = $calendarService;
    }

    /**
     * @return CalendarResponse
     */
    public function timeline(Station $station) : CalendarResponse
    {
        $timeline = CalendarDTO::collectionFromDomain(
            $this->calendarService->timeline(
                $station->id
            )
        );

        return new CalendarResponse($timeline);
    }
}
