<?php

namespace App\Http\Controllers;

use App\Service\StationService;
use App\Util\ResponseStatusCodeUtil;
use Illuminate\Http\JsonResponse;

class StationController extends Controller
{
    protected StationService $stationService;

    public function __construct(StationService $stationService)
    {
        $this->stationService = $stationService;
    }

    /**
     * @return JsonResponse
     */
    public function index() :JsonResponse
    {
        $payload = [
            'data' => $this->stationService->getAll()
        ];

        return $this->response($payload, ResponseStatusCodeUtil::HTTP_OK);
    }
}
