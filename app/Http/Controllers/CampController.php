<?php

namespace App\Http\Controllers;

use App\Service\CampService;
use App\Util\ResponseStatusCodeUtil;
use Illuminate\Http\JsonResponse;

class CampController extends Controller
{
    // change private
    protected CampService $campService;

    public function __construct(CampService $campService)
    {
        $this->campService = $campService;
    }

    /**
     * @return JsonResponse
     */
    public function index() :JsonResponse
    {
        $payload = [
            'data' => $this->campService->getAll()
        ];

        return $this->response($payload, ResponseStatusCodeUtil::HTTP_OK);
    }
}
