<?php

namespace App\Http\Controllers;

use App\Service\EquipmentService;
use App\Util\ResponseStatusCodeUtil;
use Illuminate\Http\JsonResponse;

class EquipmentController extends Controller
{
    protected EquipmentService $equipmentService;

    public function __construct(EquipmentService $equipmentService)
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $payload = [
            'data' => $this->equipmentService->getAll()
        ];

        return $this->response($payload, ResponseStatusCodeUtil::HTTP_OK);
    }
}
