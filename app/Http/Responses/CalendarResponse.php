<?php

namespace App\Http\Responses;

use App\DTO\CalendarDTO;
use App\Util\InventoryUtil;
use App\Util\ResponseStatusCodeUtil;
use Illuminate\Contracts\Support\Responsable;

class CalendarResponse implements Responsable
{
    private array $timeline;

    /**
     * @param CalendarDTO[] $timeline
     */
    public function __construct(array $timeline)
    {
        $this->timeline = $timeline;
    }

    public function toResponse($request)
    {
        $formattedTimeline = [];
        foreach ($this->timeline as $dateDay => $day) {
            foreach ($day as $equipment) {
                $formattedTimeline[$dateDay][] = "{$equipment->getEquipment()}: today: {$equipment->getToday()} / balance: ".($equipment->getBalance() + InventoryUtil::INITIAL_QUANTITY);
            }
        }

        return response()->json($formattedTimeline, ResponseStatusCodeUtil::HTTP_OK);
    }
}
