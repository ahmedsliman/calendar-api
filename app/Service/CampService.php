<?php

namespace App\Service;

use App\Repository\CampRepository;

class CampService extends BaseService
{
    protected $repository;

    /**
     * @param CampRepository $repository
     */
    public function __construct(CampRepository $repository)
    {
        $this->repository = $repository;
    }
}
