<?php

namespace App\Service;

use App\Repository\StationRepository;

class StationService extends BaseService
{
    protected $repository;

    /**
     * @param StationRepository $repository
     */
    public function __construct(StationRepository $repository)
    {
        $this->repository = $repository;
    }
}
