<?php

namespace App\Service;

class BaseService
{
    protected $repository;

    public function getAll()
    {
        return $this->repository->all();
    }
}
