<?php

namespace App\Service;

use App\DTO\CalendarDTO;
use App\Repository\CalendarRepository;

class CalendarService
{
    private CalendarRepository $repository;

    /**
     * @param CalendarRepository $repository
     */
    public function __construct(CalendarRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $stationId
     * @return CalendarDTO[]
     */
    public function timeline(int $stationId) :array
    {
        $timeline = [];

        foreach ($this->repository->quantitiesPerDay($stationId) as $day) {
            $timeline[$day->ei_date][] = new CalendarDTO(
                $day->equipment_name,
                $day->today_quantity,
                $day->current_quantity_today
            );
        }

        return $timeline;
    }
}
