<?php

namespace App\Service;

use App\Repository\EquipmentRepository;

class EquipmentService extends BaseService
{
    protected $repository;

    /**
     * @param EquipmentRepository $repository
     */
    public function __construct(EquipmentRepository $repository)
    {
        $this->repository = $repository;
    }
}
