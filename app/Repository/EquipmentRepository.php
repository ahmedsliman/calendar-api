<?php

namespace App\Repository;

use App\Models\Equipment;

class EquipmentRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Equipment::class;
    }
}
