<?php

namespace App\Repository;

use Illuminate\Support\Facades\DB;

class CalendarRepository
{
    public function quantitiesPerDay(int $stationId): array
    {
        return DB::table('equipment_inventory AS ei')
            ->select([
                'equipments.name as equipment_name',
                DB::raw('DATE(ei.order_date) AS ei_date'),
                'ei.equipment_id AS e_id',
                DB::raw('sum(ei.quantity) AS today_quantity'),
                DB::raw('(SELECT sum(quantity) FROM equipment_inventory WHERE station_id = '.$stationId.' and DATE(order_date) <= ei_date and equipment_id = e_id) as current_quantity_today'),
            ])
            ->where('station_id', $stationId)
            ->join('equipments', 'equipments.id', '=', 'ei.equipment_id')
            ->groupBy(['ei_date', 'e_id'])
            ->orderBy('ei_date', 'asc')
            ->orderBy('e_id', 'asc')
            ->get()
            ->toArray();
    }
}
