<?php

namespace App\Repository;

use App\Models\Camp;

class CampRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Camp::class;
    }
}
