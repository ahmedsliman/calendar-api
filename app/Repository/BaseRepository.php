<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * @var Model|null
     */
    protected $model = null;

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @return string
     */
    abstract public function model(): string;

    /**
     * @return Model|null
     */
    protected function makeModel(): ?Model
    {
        if (is_null($this->model)) {
            $this->model = app($this->model());
        }
        return $this->model;
    }

    /**
     * @return Collection|Model[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @return Collection|Model[]
     */
    public function getAll(int $perPage = 10)
    {
        return $this->model->paginate($perPage);
    }
}
