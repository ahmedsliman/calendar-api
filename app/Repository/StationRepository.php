<?php

namespace App\Repository;

use App\Models\Station;

class StationRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Station::class;
    }
}
