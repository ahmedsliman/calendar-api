<?php

namespace App\DTO;

class CalendarDTO implements \JsonSerializable
{
    private string $equipment;
    private int $today;
    private int $balance;

    public function __construct(string $equipment, string $today, string $balance)
    {
        $this->equipment = $equipment;
        $this->today = $today;
        $this->balance = $balance;
    }

    /**
     * @param array $equipments
     * @return array
     */
    public static function collectionFromDomain(array $timeline): array
    {
        $items = [];
        foreach ($timeline as $dayDate => $day) {
            foreach ($day as $equipment) {
                $items[$dayDate][] = new static($equipment->equipment, $equipment->today, $equipment->balance);
            }
        }

        return $items;
    }

    public function toArray(): array
    {
        return [
            'equipment'  => $this->getEquipment(),
            'today'    => $this->getToday(),
            'balance'  => $this->getBalance(),
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @return string
     */
    public function getEquipment(): string
    {
        return $this->equipment;
    }

    /**
     * @return int
     */
    public function getToday(): int
    {
        return $this->today;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }
}
