# Timeline API Task

## Notes.

- I added some layers that may not usually exist in small tasks just to show my preferences.
- Feature tests usually cover most scenarios and unit tests should cover more advanced ones. not usually test Model layers.

## To make the application ready:

-------------------

#### Download repo.
- `git clone https://github.com/ahmedsliman/calendar-url.git`
- `cd calendar-url`

#### Rename .env.example file to .env

#### Download dependencies
- `composer install`

#### Create Databases
- `create database calendar`
- `create database calendar_testing`

#### Run these commads:

- `php artisan migrate`
- `php artisan db:seed`
- `php artisan serve`


### References Endpoints

-------------------

**Camps** `<url>/api/camps`

**Stations** `<url>/api/stations`

**Equipments** `<url>/api/equipments`

## Calendar Endpoint

-------------------

`<url>/api/calendar/{Station ID}`

**Example** `<url>/api/calendar/1`

## To run tests:
`./vendor/bin/phpunit`
