<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RoadSurfer</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    </head>
    <body class="antialiased">
        <div class="sm:items-center py-4">

            <h2 id="roadsurfer-task">RoadSurfer Task</h2>
            <h3 id="to-make-the-application-ready">To make the application ready:</h3>
            <hr />
            <ul>
                <li><code>php artisan migrate</code></li>
                <li><code>php artisan db:seed</code></li>
                <li><code>php artisan serve</code></li>
            </ul>
            <h3 id="references-endpoints">References Endpoints</h3>
            <hr />
            <p><strong>Camps</strong> <code>&lt;url&gt;/api/camps</code></p>
            <p><strong>Stations</strong> <code>&lt;url&gt;/api/stations</code></p>
            <p><strong>Equipments</strong> <code>&lt;url&gt;/api/equipments</code></p>
            <h3 id="calendar-endpoint">Calendar Endpoint</h3>
            <hr />
            <p><code>&lt;url&gt;/api/calendar/1</code></p>
        </div>
    </body>
</html>
