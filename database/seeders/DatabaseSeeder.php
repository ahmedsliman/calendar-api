<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * @var array|string[]
     */
    private array $initialStations = ['Munich', 'Paris', 'Porto', 'Madrid'];

    /**
     * @var array|string[]
     */
    private array $initialEquipments = ['portable toilets', 'bed sheets', 'sleeping bags', 'camping tables', 'chairs'];

    /**
     * @var array|string[]
     */
    private array $initialCamps = ['camp 1', 'camp 2', 'camp 3', 'camp 4'];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateAllTables();

        $this->seedStations();
        $this->seedEquipments();
        $this->seedCamps();

        // Orders
        $this->seedOrders($this->campsIds(), $this->stationsIds(), $this->equipmentsIds());
    }

    /**
     * @return void
     */
    public function seedStations(): void
    {
        $stations = [];
        foreach ($this->initialStations as $station) {
            $stations[] = ['name' => $station];
        }
        DB::table('stations')->insertOrIgnore($stations);
    }

    /**
     * @return void
     */
    public function seedEquipments(): void
    {
        $equipments = [];
        foreach ($this->initialEquipments as $equipment) {
            $equipments[] = ['name' => $equipment];
        }
        DB::table('equipments')->insert($equipments);
    }

    /**
     * @return void
     */
    public function seedCamps(): void
    {
        $camps = [];
        foreach ($this->initialCamps as $camp) {
            $camps[] = ['name' => $camp];
        }
        DB::table('camps')->insert($camps);
    }

    /**
     * @param array $campsIds
     * @param array $stationsIds
     * @param array $equipmentsIds
     * @return void
     */
    public function seedOrders(array $campsIds, array $stationsIds, array $equipmentsIds): void
    {
        for ($x = 0; $x <= 10; $x++) {
            $orderId = DB::table('orders')->insert([
                'camp_id' => array_rand($campsIds),
                'start_station_id' => $startStation = array_rand($stationsIds),
                'end_station_id' => $endStation = array_rand($stationsIds),
                'rental_start_date' => $rentalDate = Carbon::today()->addDay($x)->toDateString(),
                'rental_end_date' => $returnDate = Carbon::today()->addDay($x + rand(5, 15))->toDateString(),
            ]);

            for ($eq = 0; $eq <= rand(1, 3); $eq++) {
                $quantity = rand(1, 7);
                // rental
                DB::table('equipment_inventory')->insert([
                    'order_id' => $orderId,
                    'order_date' => $rentalDate,
                    'station_id' => $startStation,
                    'equipment_id' => $equipmentsIds[$eq],
                    'quantity' => -($quantity),
                    'created_at' => Carbon::today()->toDateString(),
                ]);

                // return
                DB::table('equipment_inventory')->insert([
                    'order_id' => $orderId,
                    'order_date' => $returnDate,
                    'station_id' => $endStation,
                    'equipment_id' => $equipmentsIds[$eq],
                    'quantity' => $quantity,
                    'created_at' => Carbon::today()->toDateString(),
                ]);
            }
        }
    }

    /**
     * @return array
     */
    public function campsIds(): array
    {
        return DB::table('camps')->pluck('id')->toArray();
    }

    /**
     * @return array
     */
    public function stationsIds(): array
    {
        return DB::table('stations')->pluck('id')->toArray();
    }

    /**
     * @return array
     */
    public function equipmentsIds(): array
    {
        return DB::table('equipments')->pluck('id')->toArray();
    }

    /**
     * @return void
     */
    private function truncateAllTables() :void
    {
        $dbStr = "Tables_in_".DB::connection()->getDatabaseName();
        $tables = DB::select('SHOW TABLES');

        foreach($tables as $table){
            if ($table == 'migrations') continue;
            DB::table($table->$dbStr)->truncate();
        }
    }
}
