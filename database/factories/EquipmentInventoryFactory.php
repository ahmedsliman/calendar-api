<?php

namespace Database\Factories;

use App\Models\Camp;
use App\Models\Order;
use App\Models\Station;
use Illuminate\Database\Eloquent\Factories\Factory;

class EquipmentInventoryFactory extends Factory
{
    /**
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => Order::factory()->create()->id,
            'order_date' => 0,
            'station_id' => 0,
            'equipment_id' => 0,
            'quantity' => 0,
        ];
    }
}
