<?php

namespace Database\Factories;

use App\Models\Camp;
use App\Models\Station;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * @return array
     */
    public function definition()
    {
        return [
            'camp_id' => Camp::factory()->create()->id,
            'start_station_id' => Station::factory()->create()->id,
            'end_station_id' => Station::factory()->create()->id,
            'rental_start_date' => $this->faker->date(),
            'rental_end_date' => $this->faker->date(),
        ];
    }
}
